#!/usr/bin/env node
'use strict';

var exec = require('child_process').execSync;
var packageJson = require('../package.json');

for( var plugin in packageJson.dependencies){
    var version =  packageJson.dependencies[plugin];
    exec('npm install ' + plugin+ '@' + version);
}

var colors = require('colors');
var fs = require('fs-extra');

function fileExists(path) {
  try  {
    return fs.statSync(path).isFile();
  }
  catch (e) {
    return false;
  }
}

function directoryExists(path) {
  try  {
    return fs.statSync(path).isDirectory();
  }
  catch (e) {
    return false;
  }
}

if (directoryExists("platforms/ios")) {
  fs.copy('res/gtm/', 'platforms/ios/', function (err) {
      if (err) return console.error(err.red + ' An error occured! did you put your assets in; "res/gtm"? \r\n'.red);

      process.stdout.write('Resources successfully copied! \r\n'.green)
  });
  fs.copy('res/gtm/', 'platforms/ios/Supporting Files', function (err) {
      if (err) return console.error(err.red + ' An error occured! did you put your assets in; "res/gtm"? \r\n'.red);

      process.stdout.write('Resources successfully copied! \r\n'.green)
  });
}

if (directoryExists("platforms/android")) {
  fs.copy('res/gtm/', 'platforms/android/res/raw', function (err) {
      if (err) return console.error(err.red + ' An error occured! did you put your assets in; "res/gtm"? \r\n'.red);

      process.stdout.write('Resources successfully copied! \r\n'.green)
  });
}
